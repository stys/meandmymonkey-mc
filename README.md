** Me & My Monkey **

*** Use from command line ***

	:::shell
	D:\path\to\file> meandmymonkey-mc x y S 


Arguments

`x` - probability of winnig our offence

`y` - probability of opponent team winning offence

`S` - maximum score (i.e. 11)

Example

	:::shell 
    D:\path\to\file> meandmymonkey-mc 0.2 0.3 11 

Ouput

	:::shell
	0.23  # probability of winnig 	
	0.77  # probability of loosing


For probabilities of final scores see `P.txt`

	:::shell
	0.007 0.014	 # probabilities of 11:0 and 0:11
	0.023 0.061	 # probabilities of 11:1 and 1:11
	...


*** Build from source ***

Build with Visual Studio Express 2010