﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace com.stys.monkey
{
    class Program
    {
        /// <summary>
        /// Monte-Carlo calculation of final score probability distribution.
        /// arg[0] - probability of winning an offence
        /// arg[1] - probability of loosing a defence
        /// arg[2] - final score
        /// 
        /// Approximations: 
        /// 1) Game is always played to final score (no time limit)
        /// 2) No tie-breaks at the end of the game
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            // probability of winning offence
            double x = double.Parse(args[0]);
            if (x < 0 || x > 1.0) throw new ArgumentOutOfRangeException("x", "Should be in [0 ... 1]");
    
            // probability of loosing defence
            double y = double.Parse(args[1]);
            if (y < 0 || y > 1.0) throw new ArgumentOutOfRangeException("y", "Should be in [0 ... 1]");

            // final score
            int S = int.Parse(args[2]);
            if (S < 1) throw new ArgumentOutOfRangeException("S", "Should be no less than 1");
            
            // table of score probabilities
            double[] win = new double[S];
            double[] los = new double[S];

            // Monte-Carlo runs
            int N = 10000;
            Random rnd = new Random();
            for (int j = 0; j < N; j++)
            {
                double r = rnd.NextDouble();
                int s = 0; // score of our team
                int z = 0; // scroe of their team
                bool offence = r < 0.5; // always start with our offence
                while (s < S && z < S)
                {
                    // generate random number
                    if (offence)
                    {
                        r = rnd.NextDouble();
                        if (r < x) // win 
                        {
                            s += 1; // increment our score
                        }
                    }
                    else
                    {
                        r = rnd.NextDouble();
                        if (r < y) // loose
                        {
                            z += 1;
                        }
                    }

                    // in any case offence team changes
                    offence = !offence;
                }

                // put final score into probability
                if (s == S) win[z] += 1;
                if (z == S) los[s] += 1;
            }
            
            // write result to text file
            using (var writer = new StreamWriter("P.txt"))
            {
                for (int j = 0; j < S; j++)
                {
                    writer.Write("{0}\t{1}\n", win[j]/N, los[j]/N);
                }
            }

            Console.Out.WriteLine(win.Sum()/N);
            Console.Out.WriteLine(los.Sum()/N);
            Console.In.ReadLine();
        }
    }
}
